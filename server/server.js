const AWS = require('aws-sdk');
const { v4: uuid } = require('uuid');
const fs = require('fs');
const fastify = require('fastify')({
  logger: true,
  http2: true,
  https: {
    key: fs.readFileSync('/Users/cmendoza/cert/key.pem'),
    cert: fs.readFileSync('/Users/cmendoza/cert/cert.pem'),
  }});

require("dotenv").config();

// You must use "us-east-1" as the region for Chime API and set the endpoint.
const chime = new AWS.Chime({ region: 'us-east-1' });
chime.endpoint = new AWS.Endpoint('https://service.chime.aws.amazon.com');

// const app = fastify();

fastify.register(require('fastify-cors'), {
  methods: ['GET', 'POST', 'OPTIONS', 'PUT', 'PATCH']
})

fastify.get('/', (request, reply) => reply.send({ hello: 'world' }))

const appointments = [];

async function getAllAppointments(request, reply) {
  return reply
    .send(appointments);
};
async function getAppointmentByMeetingId(request, reply) {
  const { meetingId } = request.query;
  console.log("looking for meeting id: ", meetingId);
  const meeting = appointments.find((a) => a.MeetingId === request.query.meetingId);
  if (meeting) {
    return reply
      .send({ Meeting: meeting });
  } else {
    return reply.code(500).send("Meeting cannot be found.");
  }
};


async function createAppointment(request, reply) {
  const appt = await chime.createMeeting({
    ClientRequestToken: uuid(),
    MediaRegion: 'us-west-2' // Specify the region in which to create the meeting.
  }).promise().then(async (result) => {
    console.log("Successfully created appointment: ", result);
    return result.Meeting;
  }).catch((error) => {
    console.log("Error creating appointment: ", error);
  });

  if (appt) {
    appointments.push(appt);

    return reply
      .send("Successfully created new appointment: ", appt.MeetingId);
  } else {
    return reply
      .send("Failed to create appointment.");
  }
};

fastify.get('/appointment', async (request, reply) => {
  switch (request.query.op) {
    case "All":
      await getAllAppointments(request, reply);
      break;
    case "ByMeetingId":
      await getAppointmentByMeetingId(request, reply);
      break;
    default:
      reply
        .code(500)
        .send("Unrecognized operation.");
  }
});
fastify.post('/appointment', async (request, reply) => {
  switch (request.query.op) {
    case "Create":
      await createAppointment(request, reply);
      break;
    default:
      reply
        .code(500)
        .send("Unrecognized operation.");
  }
});

async function requestNewAttendee(request, reply) {
  const attendeeResponse = await chime.createAttendee({
    MeetingId: request.query.meetingId,
    ExternalUserId: uuid() // Link the attendee to an identity managed by your application.
  }).promise().then((result) => {
    console.log("successfully added attendee: ", result);
    return result;
  }).catch((error) => {
    console.log("error creating attendee: ", error);
  });

  if (attendeeResponse) {
    return reply
      .send(attendeeResponse);
  } else {
    return reply
      .code(500)
      .send("Failed to create attendee.");
  }
};
fastify.post('/attendee', async (request, reply) => {
  switch (request.query.op) {
    case "RequestNew":
      await requestNewAttendee(request, reply);
      break;
  }
});


if (require.main === module) {
  // called directly i.e. "node app"
  fastify.listen(3000, '0.0.0.0', (err) => {
    if (err) console.error(err)
    console.log('server listening on 3000')
  })
} else {
  // required as a module => executed on aws lambda
  module.exports = fastify
}

