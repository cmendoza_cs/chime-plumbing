import { createApp } from 'vue';
import App from './App.vue';

import {
  Button,
  Table
} from "ant-design-vue";
import router  from "./router";

const app = createApp(App);

app.use(Button);
app.use(Table);

app
  .use(router)
  .mount('#app');
