import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Appointment from "@/components/Appointment.vue";
import HomePage from "../components/HomePage.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "home",
    component: HomePage,
    meta: {
      requiresAuth: false,
    },
    children: [
    ]
  },
  {
    path: "/appointment",
    name: "appointment",
    component: Appointment,
    meta: {
      requiresAuth: false,
    },
    children: [
    ],
    props: (route) => ({
      ...route.params
    })
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return {
        left: 0,
        top: 0,
      };
    }
  },
});

router.beforeResolve(async (to, from, next) => {
  return next();
});


export default router

