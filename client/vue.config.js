const fs = require('fs');

module.exports = {
  devServer: {
    https: true,
    key: fs.readFileSync('/Users/cmendoza/cert/key.pem'),
    cert: fs.readFileSync('/Users/cmendoza/cert/cert.pem'),
    disableHostCheck: true
  }
}
