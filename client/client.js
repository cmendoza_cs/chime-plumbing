const {ConsoleLogger,
  DefaultDeviceController,
  DefaultMeetingSession,
  LogLevel,
  MeetingSessionConfiguration} = require('amazon-chime-sdk-js');

const axios = require('axios');
require("dotenv").config();

const logger = new ConsoleLogger('ChimeMeetingLogs', LogLevel.INFO);
const deviceController = new DefaultDeviceController(logger);

async function retrieveMeeting() {
  return await axios.get(`${process.env.ENDPOINT_BASE_URL}/meeting`)
    .catch((error) => {
      console.log(error);
    });
}

async function requestCreateAttendee() {
  return await axios.get(`${process.env.ENDPOINT_BASE_URL}/attendee/new`)
    .catch((error) => {
      console.log(error);
    });
}

async function joinMeeting () {
  const meetingObj = await retrieveMeeting();
  const attendeeObj = await requestCreateAttendee();

  console.log("Retrieved meeting: ", meetingObj.data);
  console.log("Retrieved Attendee: ", attendeeObj.data);
}

joinMeeting();

const configuration = new MeetingSessionConfiguration(meeting, attendee);
const meetingSession = new DefaultMeetingSession(configuration, logger, deviceController);